# MySQL JSON Export / Import for MySQLWorkbench 8.0+

## Description

Plugin for MySQL Workbench 8.0+. Exports database model, including EER Diagram as JSON and recreates model when imported.

This plugin is designed to help database model version control which can be quite difficult as .mwb files created by MySQLWorkbench are
binary files.

### Current Version

Current version is 0.4.1

## Installation

Download `model2json_grt.py` file and place it to MySQLWorkbench user modules directory. Table below shows location for user plugin files for each supported OS.

| Operating System | File Path                                                     |
| ---------------- | ------------------------------------------------------------- |
| Windows          | %AppData%\MySQL\Workbench\modules                             |
| macOS            | ~username/Library/Application Support/MySQL/Workbench/modules |
| Linux            | ~username/.mysql/workbench/modules                            |

## Usage

**Export**

1. Open MySQL model in MySQLWorkbench
1. Generate SQL Create script by `file > export > Forward Engineer SQL CREATE Script`
1. While keeping model open go to `Tools > Model > Export model as JSON`
1. When prompted, select SQL Create script file created in step 2.
1. Plugin will create `.json` file to same location and name as model `.mwb` file. If model binary file is named `mydbmodel.mwb`, exported JSON file will be `mydbmodel.json`

**Import**

1. Open or create MySQL model in MySQLWorkbench
2. Go to `Tools > Model > Import model from JSON`
3. When prompted, select `.json` file (ie. `mydbmodel.json`)

## Known Limitations

Plugin is developed and tested on MySQLWorkbench 8.0.15. Older versions are not supported.

Plugin is developed only for MySQL dialect in mind. Other database providers are not tested.

Current version does not restore all diagram figures, supported are

- Layers
- Tables
- Views
- Connections
