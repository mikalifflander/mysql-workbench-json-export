# import the wb module
from wb import DefineModule, wbinputs

# import the grt module
import grt

# import the mforms module for GUI stuff
import mforms

# Import json and OS modules for JSON formatted
# file writes and reads
import json
import os

# define this Python module as a GRT module
ModuleInfo = DefineModule(
    name="Model2JSON", author="Mika Lifflander", version="0.4.1")

######################################
#           Model to JSON            #
######################################


@ModuleInfo.plugin("fi.mikalifflander.wb.model2json",
  caption="Export model as JSON",
  input=[wbinputs.currentModel()],
  groups=["Menu/Model"])
@ModuleInfo.export(grt.INT, grt.classes.workbench_physical_Model)
def export_model_as_json(model):

    # Path for current document
    docpath = grt.root.wb.info.owner.docPath

    # Form model.json filename with path
    export_path = docpath[:-3] + 'json'
    grt.log_info('Model Export',
                 'JSON export file is {fn}\n'.format(fn=export_path))

    # Let's open the exported create script
    script_path = grt.modules.Workbench.requestFileOpen('', 'sql')

    if script_path is None or len(script_path) == 0:
      grt.log_info('Model Export', 'Export cancelled by user\n')
      mforms.App.get().set_status_text('JSON export cancelled by user')
      return 0

    # Split commands to separate lines
    # This makes is easier to compare file contents with version control
    scripts_array = read_file(script_path).split('\n')

    # Get list of diagrams in current model
    diagrams = diagrams_to_dict(model)

    # Format export object
    json_contents = {
      "diagrams": diagrams,
      "script": scripts_array
    }

    # Export to file
    write_to_file(export_path, json.dumps(
        json_contents, indent=2, sort_keys=True))

    mforms.App.get().set_status_text('JSON export completed')
    return 0


def diagrams_to_dict(model):
  """Reads diagrams from given model and
  creates dictionary presentation of them

  Arguments:
    model {grt.classes.workbench_physical_Model} -- Physical Workbench model

  Returns:
    [list] -- List of diagrams with content in dict type objects
  """

  diagrams = []

  for diagram in model.diagrams:
    grt.log_info('diagrams2dict',
                 'Found diagram {d} in model\n'.format(d=diagram.name))

    diag = {
      "name": diagram.name,
      "figures": figures_to_dict(diagram.rootLayer.figures),
      "layers": layers_to_dict(diagram.layers),
      "connections": connections_to_dict(diagram.connections)
    }

    diagrams.append(diag)

  return diagrams


def layers_to_dict(diagLayers):
  """Reads given array of layers and creates dictionary
     formatted presentation of each layer

  Arguments:
    diagLayers {list<grt.classes.model_Layer} -- List of layers to be parsed

  Returns:
    list<dict> -- List of dictionaries containing configuration for each layer
  """

  layers = []

  for layer in diagLayers:
    layers.append({
      "name": layer.name,
      "top": layer.top,
      "left": layer.left,
      "color": layer.color,
      "description": layer.description,
      "height": layer.height,
      "width": layer.width,
      "figures": figures_to_dict(layer.figures),
    })

  return layers


def figures_to_dict(layerFigures):
  """Read given list of figures and creates dictionary
     presentation of each element

  Arguments:
    layerFigures {list<grt.classes.model_Figure} -- List of figures

  Returns:
    list<dict> -- List of dictionary objects containing configration for each figure
  """

  figures = []

  for figure in layerFigures:
    _fig = figure_to_dict(figure)

    if _fig is not None:
      figures.append(_fig)
      continue

    figures.append({
      "name": figure.name,
      "expanded": figure.expanded,
      "locked": figure.locked,
      "top": figure.top,
      "left": figure.left,
      "height": figure.height,
      "width": figure.width,
      "color": figure.color,
      "locked": figure.locked,
      "visible": figure.visible,
      "manualSizing": figure.manualSizing
    })

  return figures


def figure_to_dict(figure):
  """Converts one diagram figure to dictionary representation based on
    figure type
  
  Arguments:
    figure {grt.classes.model_Figure} -- Diagram figure

  Returns:
    Dict -- Dictionary presentation of given figure
  """

#  grt.log_info('figure2dict', 'figure of grt class {cn} is of type TableFigure {cb}\n'.format(
 #     cn=figure.__grtclassname__, cb=isinstance(figure, grt.classes.workbench_physical_TableFigure)))

  if isinstance(figure, grt.classes.workbench_model_NoteFigure):
    return get_note_figure(figure)

  if isinstance(figure, grt.classes.workbench_physical_TableFigure):
    return get_table_figure(figure)

  return None


def get_table_figure(figure):
  figure_dict = get_basic_figure_dict(figure)

  # grt.log_info('getTableFigure', 'Table inserts {ti}\n'.format(ti=figure.table.inserts()))

  inserts = figure.table.inserts().split('\n')

  columns = map(lambda c: c.name, figure.table.columns)

  # Dictionary to hold all inserts per column
  inserts_dict = {
    "rowCount": len(inserts), 
    "colCount": len(columns),
    "columns": {}
  }

  # Loop through all insert rows
  for insert in inserts: 

      if not insert or len(insert) == 0:
        # Empty inserts row must be excluded from total count
        inserts_dict['rowCount'] = inserts_dict['rowCount'] - 1
        continue

      # Lets find actual values 
      s_index = insert.index('(') + 1
      s_index = insert.index('(', s_index) + 1
      e_index = insert.index(')', s_index)

      values = insert[s_index:e_index].split(',')

      for i in range(0, len(values)):
        # Try to get previously allocated dictionary
        try:
          ins_dict = inserts_dict['columns'][columns[i]]
        except KeyError:
          # Create new if not found
          ins_dict = {
            "values": []
          }

        value = values[i]

        # Figure out value type. Needed when importing
        # inserts from JSON
        try:
          index = value.index('\'')
          ins_dict['values'].append(value.strip())
          ins_dict['type'] = 'string'
        except ValueError:
          try:
            int_value = int(value.strip())
            ins_dict['values'].append(int_value)
            ins_dict['type'] = 'int'
          except ValueError:
            try:
              float_value = float(value.strip())
              ins_dict['values'].append(float_value)
              ins_dict['type'] = 'float'
            except ValueError:
              ins_dict['values'].append('')
              ins_dict['type'] = 'NULL'
        
        # Map dictionary by  column name
        inserts_dict['columns'][columns[i]] = ins_dict

      figure_dict['inserts'] = inserts_dict

  return figure_dict

def get_note_figure(figure):
  figure_dict = get_basic_figure_dict(figure)

  figure_dict['font'] = figure.font
  figure_dict['textColor'] = figure.textColor
  figure_dict['text'] = figure.text
  figure_dict['type'] = 'NoteFigure'

  return figure_dict

def get_basic_figure_dict(figure):
  return {
      "name": figure.name,
      "expanded": figure.expanded,
      "locked": figure.locked,
      "top": figure.top,
      "left": figure.left,
      "height": figure.height,
      "width": figure.width,
      "color": figure.color,
      "locked": figure.locked,
      "visible": figure.visible,
      "manualSizing": figure.manualSizing
    }

def connections_to_dict(connections):
  """Reads diagram connections and creates dictionary presentation
     of each element
  
  Arguments:
    connections {list<workbench_physical_Model>} -- List of physical connections
  
  Returns:
    [list<dict>] -- list of dictionary presentations of given connections
  """

  conns = []

  for con in connections:
    conns.append({
      "name": con.name,
      "caption": con.caption,
      "captionXOffs": con.captionXOffs,
      "captionYOffs": con.captionYOffs,
      "comment": con.comment,
      "endCaptionXOffs": con.endCaptionXOffs,
      "endCaptionYOffs": con.endCaptionYOffs,
      "extraCaption": con.extraCaption,
      "extraCaptionXOffs": con.extraCaptionXOffs,
      "extraCaptionYOffs": con.extraCaptionYOffs,
      "middleSegmentOffset": con.middleSegmentOffset,
      "startCaptionXOffs": con.startCaptionXOffs,
      "startCaptionYOffs": con.startCaptionYOffs,
    })

  return conns


######################################
#           JSON to model            #
######################################

@ModuleInfo.plugin("fi.mikalifflander.wb.json2model", 
  caption= "Import model from JSON", 
  input= [wbinputs.currentModel()], 
  groups=["Menu/Model"])

@ModuleInfo.export(grt.INT, grt.classes.workbench_physical_Model)
def import_json_to_model(model):

  # Read configuration file
  def_file = grt.modules.Workbench.requestFileOpen('', 'json')

  # Handle cancel button
  if def_file is None or len(def_file) == 0:
    grt.log_info('json2model', 'Import cancelled by user\n')
    mforms.App.get().set_status_text('JSON import cancelled by user')
    return

  # Load JSON to dictionary
  config = json.loads(read_file(def_file))

  # Import configuration to current catalog
  if config_to_catalog(config, model.catalog) > 0:
    return

  # Remove all diagrams
  model.diagrams.remove_all()

  # Create new diagrams
  json_to_diagrams(config, model)

  mforms.App.get().set_status_text('JSON import completed')
  return 0

def json_to_diagrams(config, model):
 
  for diagram in config['diagrams']:
    json_to_diagram(diagram, model)


def json_to_diagram(config, model):
  """Converts configuration object to physical diagram
  
  Arguments:
    config {dict} -- Diagram configuration
    model {grt.classes.workbench_physical_Model} -- Target physical model
  """
  # Index for new diagram is same as current count
  diagram_count = len(model.diagrams)

  # Create new diagram
  grt.modules.WbModel.createDiagramWithCatalog(model, model.catalog)

  # Configure newly created diagram
  diagram = model.diagrams[diagram_count]
  diagram.name = config['name']

  # Arrange figures
  arrange_layers_to_diagram(config['layers'], diagram)
  configured =  arrange_figures_to_diagram(config, diagram)

  # Remove unwanted figures
  to_remove = [f for f in diagram.figures if f.name not in configured]
  
  for f in to_remove:
    diagram.removeFigure(f)

  # Gather connections from configuration and connections from diagram 
  # that are not included in configuration
  connections = map(lambda c: c['caption'], config['connections']) 
  removable_connections = [c for c in diagram.connections if c.caption not in connections]

  # Remove unwanted connections from diagram
  for rc in removable_connections:
    diagram.connections.remove(rc)

def arrange_layers_to_diagram(config, diagram):
  """Iterates through layer configuration and adds all layers to diagram
  
  Arguments:
    config {List<Dict>} -- list of layer configuration dictionaries
    diagram {grt.classes.model_Diagram} -- Targat physical diagram for layers
  """
  for layer in config:
      # grt.log_info('DiagramImport', 'Found layer ' + layer['name'] + '\n')
      _layer = diagram.placeNewLayer(layer['left'], layer['top'], layer['width'], layer['height'], layer['name'])
      _layer.color = layer['color']
      _layer.description = layer['description']

def arrange_figures_to_diagram(config, diagram):
  """Arranges figures to given diagram as set in configuration dictionary
  
  Arguments:
    config {dict} -- Configuration dictionary
    diagram {grt.class.model_Diagram} -- Target workbench diagram
  
  Returns:
    list<string> -- List of names of each figure that was arranged to diagram
  """

  configured = []
  arrange_figures_to_layer(config['figures'], diagram.rootLayer, diagram, configured)
  
  grt.log_info('figures2diagram', 'After rootLayer, configured figures are {f}\n'.format(f=configured))
  for layer in config['layers']:
    arrange_figures_to_layer(layer['figures'], get_layer_for_name(diagram, layer['name']), diagram, configured)

  return configured

def arrange_figures_to_layer(figures, layer, diagram, configured):
  """Arranges figures to layers per configuration
  
  Arguments:
    figures {list<dict>} -- List of figure configuration dictionary objects
    layer {grt.classes.model_Layer} -- Target layer
    diagram {grt.classes.model_Diagram} -- Source diagram holding references to all figures
    configured {list<string>} -- List of names of each figure found on diagram. Updated by this method
  """

  for figure in figures:
    grt.log_info('arrangeFiguresToLayer', 'Arranging figure {fname} to layer {lname} \n'.format(fname=figure['name'], lname=layer.name))

    _figure = get_figure_for_name(diagram, figure['name'])

    if _figure is None:
      continue

      try:
        fig_type = figure['type']
        grt.log_info('figures2layer', 'Figure is none, configured type is {ft}\n'.format(ft=fig_type))
        
        if fig_type == 'NoteFigure':
          _figure = grt.classes.workbench_model_NoteFigure()
          _figure.text = figure['text']
          _figure.textColor = figure['textColor']
          _figure.font = figure['font']
          _figure.name = figure['name']
          _figure.layer = layer
          
          # diagram.addFigure(_figure)

      except KeyError:
        grt.log_info('figures2layer', 'Figure is none and configuration does not have type\n')
        continue
    
    _figure.top = figure['top'] + layer.top
    _figure.left = figure['left'] + layer.left
    _figure.height = figure['height']
    _figure.width = figure['width']
    _figure.color = figure['color']
    _figure.layer = layer

    try:
      if isinstance(_figure, grt.classes.workbench_physical_TableFigure) and len(figure['inserts']) > 0:
        import_inserts(_figure, figure['inserts'])
    except KeyError:
      pass

    currents = map(lambda f: f.name, layer.figures)

    if _figure.name not in currents:
      layer.figures.append(_figure)
    
    if len(layer.color) > 0:
      # This is not root layer
      diagram.rootLayer.figures.remove(_figure)

    configured.append(_figure.name)

def import_inserts(figure, inserts):
  # grt.log_info('importInserts', 'Importing {rc} inserts with {cc} columns\n'.format(rc=inserts['rowCount'], cc=inserts['colCount']))
  editor = figure.table.createInsertsEditor()

  col_inserts = inserts['columns']
  for row in range(inserts['rowCount']):
    editor.addNewRow()

    for column, value in col_inserts.items():
      #grt.log_info('importInserts', 'Column conf for {col} is {cc}\n'.format(
      #    rc=inserts['rowCount'], col=column, cc=col_inserts[column]))

      col_value = value['values'][row]

      if value['type'] == 'int':
        editor.setIntFieldValueByName(column, col_value)
      if value['type'] == 'NULL':
        editor.setFieldNullByName(column)
      if value['type'] == 'float':
        editor.setFloatFieldValueByName(column, col_value)
      if value['type'] == 'string':
        editor.setStringFieldValueByName(column, col_value)
    
    editor.applyChanges()
  
def config_to_catalog(config, catalog):
  """Reads script from config object, creates one create script and imports items to given catalog

  Arguments:
    config {dict} -- Configuration dictionary
    catalog {grt.classes.model_Catalog} -- Target workbench catalog for import
  """

  # Let's check that we actually have script section in our config
  if config['script'] is None or not isinstance(config['script'], list):
    grt.log_error('json2model', 'Could not find script section')
    return 1

  # Create single string presentation of SQL script
  script = '\n'.join([str(x) for x in config['script']])
  
  # Create new context for MySQL Parser
  context = grt.modules.MySQLParserServices.createNewParserContext(catalog.characterSets, grt.root.wb.info.version, '', True)

  # Parse SQL Create script to given catalog
  grt.modules.MySQLParserServices.parseSQLIntoCatalogSql(context, catalog, script, {})

  return 0

#######################################
#  Utils for getting objects by name  #
#######################################

def get_figure_config_for_name(layerConfig, name): 
  for layer in layerConfig:
    for figure in layer['figures']:
      if figure['name'] == name:
        return figure
  return None

def get_figure_for_name(diagram, name):

  for f in diagram.figures:
    if f.name == name:
      return f
  
  return None

def get_layer_for_name(diagram, name):
  for layer in diagram.rootLayer.subLayers:
    if layer.name == name:
      return layer

  return None

def get_connection_for_caption(diagram, caption):
  for con in diagram.connections:
    if con.caption == caption:
      return caption
  return None

#######################################
#          File operations            #
#######################################

def write_to_file(filepath, data):
  if not filepath or len(filepath) == 0:
      return

  f = open(filepath, 'w')

  f.write(data)


def read_file(filepath):
  if not filepath or len(filepath) == 0:
    return None

  f = open(filepath, 'r')
  script = f.read()

  return script
